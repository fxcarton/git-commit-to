#!/bin/bash

. "$(git --exec-path)/git-sh-setup"
export GIT_DIR

msg=""

while [ "$#" -gt 0 ]; do
    case "$1" in
        -m) shift; msg="$1";;
        *) break;;
    esac
    shift
done

[ "$#" -eq 1 ] || die 'Usage: git commit-to [-m message] targetbranch'
branch="$1"

patch="$(git diff --cached)" || die "Failed to get patch."
[ -n "$patch" ] || die "Nothing to commit."

if [ -z "$msg" ]; then
    {
        cat << EOF

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
EOF
        git status | sed "/^Changes to be committed/s/:\$/ ON BRANCH $branch:/;s/^/# /"
    } > "$GIT_DIR/COMMIT_EDITMSG"
    "${EDITOR:-vim}" "$GIT_DIR/COMMIT_EDITMSG"
    msg="$(grep -v '^#' "$GIT_DIR/COMMIT_EDITMSG")"
    [ -n "$msg" ] || die 'Aborting commit due to empty commit message.'
fi

git read-tree --reset "$branch" || die "Failed to reset index to $branch"
{
    git apply --cached <<< "$patch" \
    && tree="$(git write-tree)" \
    && parent="$(git show -s --pretty=format:%h "$branch")" \
    && h="$(git commit-tree -p "$parent" -m "$msg" "$tree")" \
    && git update-ref "refs/heads/$branch" "$h" \
    && {
        git read-tree --reset HEAD
        git show -s --pretty="format:[git %h] %s%n Date: %ad" "$h"
        true
    }
} || {
    git read-tree --reset HEAD && git apply --cached <<< "$patch"
    die "Failed to commit."
}
